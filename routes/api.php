<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user/login', 'AuthController@login');
Route::post('/user/logout', 'AuthController@logout');
Route::post('/company/activate/{code}','CompanyController@companySuperAdminActivation');
Route::post('/client/activate/{code}','ClientController@clientSuperAdminActivation');

Route::middleware('auth:api')->get('/user/me', 'AuthController@view');

Route::group(['middleware' => ['auth:api']], function () {
    Route::middleware('role:app-super-admin')->post('/user/register', 'AuthController@register');

    Route::middleware('permission:add-company')->post('/company','CompanyController@create');
    Route::middleware('role:app-super-admin')->get('/company','CompanyController@list');
    Route::middleware('role:app-super-admin')->get('/company/{id}','CompanyController@view');

    Route::middleware('role:company-super-admin')->post('/client','ClientController@create');
    Route::middleware('role:company-super-admin')->get('/client','ClientController@list');
    Route::middleware('role:company-super-admin')->get('/client/{id}','ClientController@view');

    Route::middleware('role:client-super-admin')->post('/site','SiteController@create');
    Route::middleware('role:client-super-admin')->post('/site/allocation','SiteController@siteAllocationRequest');
    Route::middleware('role:company-super-admin')->post('/site/allocation/update','SiteController@siteAllocationStatusUpdate');
    Route::middleware('role:company-super-admin')->get('/site/allocation/company/list','SiteController@allocationListByCompany');
    Route::get('/site','SiteController@list');
    Route::middleware('role:company-super-admin')->get('/site/list/{companyId}','SiteController@siteListByCompany');
    Route::get('/site/list/client/{clientId}','SiteController@siteListByClient');
    Route::middleware('role:company-super-admin')->post('/site/invoices','EmployeeController@listAllInvoices');
    Route::get('/site/{id}','SiteController@view');
    Route::middleware('role:company-super-admin')->get('/site/invoices/{id}','EmployeeController@invoiceView');



    Route::middleware('role:app-super-admin')->post('/city','CityController@create');
    Route::get('/city/list','CityController@list');
    Route::get('/city/{id}','CityController@view');
    Route::get('/city/list/{key}','CityController@listByState');
    Route::get('/state/list','StateController@list');

    Route::middleware('role:company-super-admin')->post('/department','DepartmentController@create');
    Route::middleware('role:company-super-admin')->get('/department/list/{id}','DepartmentController@list');
    Route::middleware('role:company-super-admin')->get('/department/{id}','DepartmentController@view');

    Route::middleware('role:company-super-admin')->post('/designation','DesignationController@create');
    Route::get('/designation/list/{id}','DesignationController@list');
    Route::get('/designation/{id}','DesignationController@view');

    Route::middleware('role:company-super-admin')->post('/employee/info','EmployeeController@create');
    Route::middleware('role:company-super-admin')->get('/employee/list/{companyId}','EmployeeController@list');
    Route::middleware('role:company-super-admin')->post('/employee/payslips','EmployeeController@listAllPayslips');
    Route::middleware('role:company-super-admin')->get('/employee/payslips/{id}','EmployeeController@payslipView');
    Route::middleware('role:company-super-admin')->get('/employee/{employeeId}','EmployeeController@view');


    Route::post('/import', 'ImportCompanyHolidayController@import');
    Route::post('/import/attendance', 'ImportEmployeeAttendanceController@import');
    Route::post('/import/employee', 'EmployeeController@import');
    Route::get('/employee/attendance/{startDate}/{endDate}', 'ImportEmployeeAttendanceController@list');
    Route::get('/employee/attendance/leaves', 'ImportEmployeeAttendanceController@listOfLeaves');


});



