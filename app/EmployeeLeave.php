<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeLeave extends BaseModel
{
    protected $table = 'employee_leave';

    protected $fillable = ['company_id','employee_id','date','leave_type','approved_by','reason','created_by','updated_by'];
}
