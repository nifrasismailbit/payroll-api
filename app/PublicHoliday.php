<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicHoliday extends BaseModel
{
    protected $table = 'public_holiday_mst';
}
