<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends BaseModel
{
    protected $table = 'company_mst';

    public function createdBy()
    {
        return $this->hasOne(User::class,'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class,'id', 'updated_by');
    }

    public function address()
    {
        return $this->hasOne(Address::class,'id', 'company_address_id');
    }

    public function contact()
    {
        return $this->hasOne(Contact::class,'id', 'company_contact_id');
    }

    public function parentCompany(){
        return $this->hasOne(Company::class,'id', 'parent_company_id');
    }
}
