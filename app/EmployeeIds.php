<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeIds extends Model
{
    protected $table = 'employee_ids';
}
