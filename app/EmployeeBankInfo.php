<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeBankInfo extends BaseModel
{
    protected $table = 'employee_bank_info';
}
