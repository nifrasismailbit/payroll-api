<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends BaseModel
{
    protected $table = 'invoices';

    protected $fillable = [
        'site_id',
        'start_at',
        'end_at',
        'invoice_amount'
    ];

    public function invoiceItems(){
        return $this->hasMany(InvoiceItem::class,'id','invoice_id');
    }
}
