<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends BaseModel
{
    protected $table = 'employee';

    public function createdBy()
    {
        return $this->hasOne(User::class,'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class,'id', 'updated_by');
    }

    public function company()
    {
        return $this->hasOne(Company::class,'id', 'company_id');
    }

    public function designation()
    {
        return $this->hasOne(Designation::class,'id', 'designation_id');
    }

}
