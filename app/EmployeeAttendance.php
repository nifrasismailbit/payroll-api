<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAttendance extends BaseModel
{
    protected $table = 'employee_attendance';

    protected $fillable = ['site_id','designation_id','employee_id','date','pay_x','created_by','updated_by','attendance_type'];

    public function createdBy()
    {
        return $this->hasOne(User::class,'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class,'id', 'updated_by');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class,'id', 'employee_id');
    }
}
