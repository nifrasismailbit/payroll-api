<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends BaseModel
{
    protected $table = 'invoice_items';

    protected $fillable = [
        'invoice_id',
        'rate',
        'particular',
        'worked_days',
        'sub_total'
    ];
}
