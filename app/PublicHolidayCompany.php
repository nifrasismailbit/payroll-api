<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class PublicHolidayCompany extends BaseModel
{
    protected $table = 'public_holiday_company_mst';

    protected $fillable = [
        'type', 'label', 'date','country','pay_x','company_id','created_by','updated_by'
    ];
}
