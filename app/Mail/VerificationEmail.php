<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'nifrasismail@gmail.com';
        $subject = 'Activation Email iGuard.in';
        $name = 'Jane Doe';

        return $this->view('verification_email')
            ->from($address, $this->data['name'])
//            ->cc($address, $name)
//            ->bcc($address, $name)
//            ->replyTo($address, $name)
            ->subject($subject)
            ->with(
                [
                    'activation_key' => $this->data['activation_key'],
                    'name' => $this->data['name'] 
                ]
            );
    }
}
