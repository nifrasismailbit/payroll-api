<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends BaseModel
{
    protected $table = 'contact_mst';

    public function createdBy()
    {
        return $this->hasOne(User::class,'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class,'id', 'updated_by');
    }

    public function address()
    {
        return $this->hasOne(Address::class,'id', 'address_id');
    }
}
