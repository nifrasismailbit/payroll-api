<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAddress extends BaseModel
{
    protected $table = 'employee_address';
}
