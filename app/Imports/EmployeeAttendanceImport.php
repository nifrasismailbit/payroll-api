<?php

namespace App\Imports;

use App\Designation;
use App\Employee;
use App\EmployeeAttendance;
use App\EmployeeLeave;
use App\Exceptions\CustomException;
use App\User;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;

class EmployeeAttendanceImport implements ToCollection, WithStartRow,WithHeadingRow
{
    private $heading;
    private $siteId;
    public function __construct($headings,$siteId)
    {
        $this->heading = $headings;
        $this->siteId = $siteId;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function collection(Collection $rows)
    {
            //validate the sheet is valid
            $validValues = ['P','NJ','WOH','A','S', 'CH'];
            $errorRecords = array();
            foreach ($rows as $row) {
                for ($i = 2; $i < count($this->heading[0][0]); $i++) {
                    $employeeData = Employee::where(['employee_code' => $row[$this->heading[0][0][0]]])->first();
                    if (!$employeeData) {
                        array_push($errorRecords, ['code' => 'invalid_employee_code', 'message' => $row[$this->heading[0][0][0]] . ' employee is not exist']);
                    } else {
                        $designation = Designation::where(['desig_name' => $row[$this->heading[0][0][1]]])->first();

                        if (!$designation) {
                            array_push($errorRecords, ['code' => 'invalid_designation_name', 'message' => $row['employee_designation_name'] . ' designation is invalid']);
                        } else {
                            $date = str_replace('_', '/', $this->heading[0][0][$i]);
                            $time = strtotime($date);
                            $dateOfRecord = date("Y-m-d H:i:s", $time);
                            $attendanceExists = EmployeeAttendance::where(['date' => $dateOfRecord, 'employee_id' => $employeeData->id])->exists();
                            if ($attendanceExists) {
                                array_push($errorRecords, ['code' => 'exists', 'message' => $date . ' record already exists for' . $row[$this->heading[0][0][0]]]);
                            }

                            if (in_array($row[$this->heading[0][0][$i]], $validValues) == false) {
                                array_push($errorRecords, ['code' => 'invalid code', 'message' => $date . ' of ' . $row[$this->heading[0][0][0]] . ' attendance code ' . $row[$this->heading[0][0][$i]] . ' is invalid']);
                            }
                        }

                    }
                }
            }

        if(count($errorRecords) == 0) {
                foreach ($rows as $row) {
                    $user = User::find(auth()->user()->id);
                    for ($i = 2; $i < count($this->heading[0][0]); $i++) {
                        $payXVal = 0.0;
                        switch ($row[$this->heading[0][0][$i]]) {
                            case 'P' :
                                $payXVal = 1.0;
                                break;
                            case 'WOH' :
                                $payXVal = 2.0;
                                break;
                            case 'A' :
                                $payXVal = 1.0;
                                break;
                            case 'NJ' :
                                $payXVal = 0.0;
                                break;
                            case 'S' :
                                $payXVal = 0.0;
                                break;
                            case 'CH' :
                                $payXVal = 1.0;
                                break;
                        }

                        $dateOfRecord = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($this->heading[0][0][$i]);

                        $employee = Employee::where(['employee_code' => $row[$this->heading[0][0][0]]])->first();
                        $designation = Designation::where(['desig_name' => $row[$this->heading[0][0][1]]])->first();

                        if ($row[$this->heading[0][0][$i]] == 'A') {
                            EmployeeLeave::create([
                                'employee_id' => $employee->id,
                                'date' => $dateOfRecord,
                                'leave_type' => 'CASUAL',
                                'company_id' => $user->company_id,
                                'created_by' => $user->id,
                                'updated_by' => $user->id,
                            ]);
                        }

                        EmployeeAttendance::create([
                            'employee_id' => $employee->id,
                            'date' => $dateOfRecord,
                            'pay_x' => $payXVal,
                            'site_id' => $this->siteId,
                            'designation_id' => $designation->id,
                            'attendance_type' => $row[$this->heading[0][0][$i]],
                            'company_id' => $user->company_id,
                            'created_by' => $user->id,
                            'updated_by' => $user->id
                        ]);
                    }
                }

        }else{
            throw new CustomException($errorRecords);
        }


            //inset the rows after the validation
//            if(count($errorRecords) == 0) {
//                foreach ($rows as $row) {
//                    $user = User::find(auth()->user()->id);
//                    for ($i = 1; $i < count($this->heading[0][0]); $i++) {
//                        $payXVal = 0.0;
//                        switch ($row[$this->heading[0][0][$i]]) {
//                            case 'P' :
//                                $payXVal = 1.0;
//                                break;
//                            case 'WOH' :
//                                $payXVal = 2.0;
//                                break;
//                            case 'A' :
//                                $payXVal = 1.0;
//                                break;
//                            case 'NJ' :
//                                $payXVal = 0.0;
//                                break;
//                            case 'S' :
//                                $payXVal = 0.0;
//                                break;
//                            case 'CH' :
//                                $payXVal = 1.0;
//                                break;
//                        }
//
//                        $date = str_replace('_', '/', $this->heading[0][0][$i]);
//                        $time = strtotime($date);
//                        $dateOfRecord = date("Y-m-d H:i:s",$time);
//
//                        $employee = Employee::where(['employee_code' => $row[$this->heading[0][0][0]]])->first();
////                        $designation = Designation::where(['desig_name' => $row['employee_designation_name']])->first();
//
//                        if($row[$this->heading[0][0][$i]] == 'A'){
//                            EmployeeLeave::create([
//                                'employee_id' => $employee->id,
//                                'date' => $dateOfRecord,
//                                'leave_type' => 'CASUAL',
//                                'company_id' => $user->company_id,
//                                'created_by' => $user->id,
//                                'updated_by' => $user->id,
//                            ]);
//                        }
//
//                        EmployeeAttendance::create([
//                            'employee_id' => $employee->id,
//                            'date' => $dateOfRecord,
//                            'pay_x' => $payXVal,
//                            'site_id' => $this->siteId,
//                            'attendance_type' => $row[$this->heading[0][0][$i]],
//                            'company_id' => $user->company_id,
//                            'created_by' => $user->id,
//                            'updated_by' => $user->id
//                        ]);
//                    }
//
//            }else{
//                throw new CustomException($errorRecords);
//            }
    }
}

