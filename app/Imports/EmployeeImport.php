<?php

namespace App\Imports;

use App\City;
use App\Designation;
use App\Employee;
use App\EmployeeAddress;
use App\EmployeeBankInfo;
use App\EmployeeSalary;
use App\Exceptions\CustomException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;

class EmployeeImport implements ToCollection, WithStartRow,WithHeadingRow
{

    private $companyId;

    public function __construct($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
    * @param Collection $rows
    */
    public function collection(Collection $rows)
    {
        $errorRecords = array();
        foreach ($rows as $row){
            $city = City::where(['name' => $row['employee_city_name']]);
            if($city->exists()){
                $designation = Designation::where(['desig_name' => $row['employee_designation_name']]);
                if($designation->exists()){

                }else{
                    array_push($errorRecords, ['code' => 'invalid_designation_name', 'message' =>  $row['employee_designation_name'] . ' designation is invalid']);
                }
            }else{
                array_push($errorRecords, ['code' => 'invalid_city_name', 'message' =>  $row['employee_city_name'] . ' city is invalid']);
            }
        }

        if(count($errorRecords) == 0) {
            foreach ($rows as $row){
                $city = City::where(['name' => $row['employee_city_name']])->first();
                $designation = Designation::where(['desig_name' => $row['employee_designation_name']])->first();
                $employee = new Employee();
                $employee->title = $row['title'];
                $employee->fname = $row['fname'];
                $employee->mname = $row['mname'];
                $employee->lname = $row['lname'];
                $employee->gender = $row['gender'];
                $employee->marital_status = $row['marital_status'];
                $employee->personal_email = $row['personal_email'];
                $employee->mobile_no = $row['mobile_no'];
                $employee->pan_number = $row['pan_number'];
                $employee->pf_number = $row['pf_number'];
                $employee->esic_number = $row['esic_number'];
                $employee->voter_id = $row['voter_id'];
                $employee->uan_number = $row['uan_number'];
                $employee->aadhar_number = $row['aadhar_number'];
                $employee->national_id_number = $row['national_id_number'];
                $employee->blood_group = $row['blood_group'];
                $employee->employee_code = $row['employee_code'];
                $employee->official_email_id = $row['official_email_id'];
                $employee->designation_id = $designation->id;
                $employee->l1_manager_id = $row['l1_manager'];
                $employee->l2_manager_id = $row['l2_manager'];
                $employee->grade = $row['grade'];
                $employee->joining_date = $row['joining_date'];
                $employee->hr_manager_id = $row['hr_manager'];
                $employee->company_id = $this->companyId;
                $employee->created_by = auth()->user()->getAuthIdentifier();
                $employee->updated_by = auth()->user()->getAuthIdentifier();

                $employeeSalary = new EmployeeSalary;
                $employeeSalary->currency = 'INR';
                $employeeSalary->band = $row['salary_band'];
                $employeeSalary->basic = $row['salary_basic'];
                $employeeSalary->hra = $row['salary_hra'];
                $employeeSalary->special_allowance = $row['salary_special_allowance'];
                $employeeSalary->uniform_allowance = $row['salary_uniform_allowance'];
                $employeeSalary->leave_travel_allowance = $row['salary_leave_travel_allowance'];
                $employeeSalary->pf_employer = $row['salary_pf_employer'];
                $employeeSalary->professional_tax = $row['salary_professional_tax'];
                $employeeSalary->tds = $row['salary_tds'];
                $employeeSalary->created_by = auth()->user()->getAuthIdentifier();
                $employeeSalary->updated_by = auth()->user()->getAuthIdentifier();

                $employeeBankInfo = new EmployeeBankInfo;
                $employeeBankInfo->bank_name = $row['bank_bank_name'];
                $employeeBankInfo->ifsc_code = $row['bank_ifsc_code'];
                $employeeBankInfo->account_type = $row['bank_account_type'];
                $employeeBankInfo->bank_account_number = $row['bank_bank_account_number'];
                $employeeBankInfo->branch_name = $row['bank_branch_name'];
                $employeeBankInfo->account_holder_name = $row['bank_account_holder_name'];
                $employeeBankInfo->branch_address = $row['bank_branch_address'];
                $employeeBankInfo->created_by = auth()->user()->getAuthIdentifier();
                $employeeBankInfo->updated_by = auth()->user()->getAuthIdentifier();

                $employeeAddress = new EmployeeAddress;
                $employeeAddress->address_type = $row['address_address_type'];
                $employeeAddress->address = $row['address_address'];
                $employeeAddress->city_id = $city->id;
                $employeeAddress->state_id = $city->state_id;
                $employeeAddress->created_by = auth()->user()->getAuthIdentifier();
                $employeeAddress->updated_by = auth()->user()->getAuthIdentifier();


                DB::transaction(function() use ($employee, $employeeSalary,$employeeBankInfo,$employeeAddress) {

                    $employee->save();

                    $employeeSalary->employee_id = $employee->id;
                    $employeeSalary->save();

                    $employeeBankInfo->employee_id = $employee->id;
                    $employeeBankInfo->save();

                    $employeeAddress->employee_id = $employee->id;
                    $employeeAddress->save();

                });
            }
        }else{
            throw new CustomException($errorRecords);
        }
    }
}
