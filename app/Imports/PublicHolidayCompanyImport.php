<?php

namespace App\Imports;

use App\PublicHolidayCompany;
use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PublicHolidayCompanyImport implements ToCollection, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            $user = User::find(auth()->user()->id);
            PublicHolidayCompany::create([
                'type'     => $row[0],
                'date'     => $row[1],
                'label'     => $row[2],
                'country'     => $row[3],
                'pay_x'     => doubleval($row[4]),
                'company_id' => $user->company_id,
                'created_by' => $user->id,
                'updated_by' => $user->id
            ]);
        }
    }
}
