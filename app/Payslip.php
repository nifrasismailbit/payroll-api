<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payslip extends BaseModel
{
    protected $table = 'payslip';

    protected $fillable = [
        'company_id',
        'employee_id',
        'start_at',
        'end_at',
        'pay_days',
        'present_days',
        'absent_days',
        'worked_on_holiday_day',
        'paid_leave_days',
        'basic',
        'hra',
        'special_allowance',
        'uniform_allowance',
        'leave_travel_allowance',
        'pf_employer',
        'professional_tax_amount',
        'tds_amount',
        'deductions',
        'arrears',
        'guaranteed_salary',
        'notes'
    ];

}
