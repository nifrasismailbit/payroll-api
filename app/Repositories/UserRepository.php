<?php


namespace App\Repositories;


use App\Mail\VerificationEmail;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;

class UserRepository
{
    private $key = "SecretKey";
    public function registerUser($data, $roleName,$companyId)
    {
        //save the user
        //deactivate account by default
        //sennd email for activation
        //activate the account one activate

        $password = $this->generateRandomString(20);
        $activationCode = $this->generateRandomString(30);
        $user = User::create([
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'country_code' => 'IN',
            'level' => 'S1',
            'password' => $password,
            'activation_code' => $activationCode
        ]);

        $user->company_id = $companyId;
        $user->save();

        $role = Role::findByName($roleName);
        if ($role) {
            $user->assignRole($role);
        }

        $this->sendActivationEmail($data, $activationCode);
    }

    function generateRandomString($length = 20)
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' .
            '0123456789';

        $str = '';
        $max = strlen($chars) - 1;

        for ($i = 0; $i < $length; $i++)
            $str .= $chars[random_int(0, $max)];

        return $str;
    }

    private function sendActivationEmail($message, string $activationCode)
    {
        $data = ['name' => $message['fname'] . " " . $message['lname'],'activation_key' => $activationCode];
        Mail::to($message['email'])->send(new VerificationEmail($data));
    }
}