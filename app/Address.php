<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends BaseModel
{
    protected $table = 'address_mst';

    public function createdBy()
    {
        return $this->hasOne(User::class,'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class,'id', 'updated_by');
    }

    public function state()
    {
        return $this->hasOne(State::class,'id', 'state_id');
    }

    public function city()
    {
        return $this->hasOne(City::class,'id', 'city_id');
    }
}
