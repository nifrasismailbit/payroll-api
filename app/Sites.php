<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sites extends BaseModel
{
    protected $table = 'sites';

    protected $fillable = ['site_id','client_id','company_id'];

    public function site(){
        return $this->hasOne(Company::class,'id', 'site_id');
    }

    public function client(){
        return $this->hasOne(Company::class,'id', 'client_id');
    }

    public function company(){
        return $this->hasOne(Company::class,'id', 'company_id');
    }
}
