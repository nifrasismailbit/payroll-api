<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class City extends BaseModel
{
    protected $table = 'city_mst';

    public function createdBy()
    {
        return $this->hasOne(User::class,'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class,'id', 'updated_by');
    }

    public function state()
    {
        return $this->hasOne(State::class,'id', 'state_id');
    }
}
