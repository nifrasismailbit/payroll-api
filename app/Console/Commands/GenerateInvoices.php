<?php

namespace App\Console\Commands;

use App\Company;
use App\Employee;
use App\EmployeeAttendance;
use App\EmployeeSalary;
use App\Invoice;
use App\InvoiceItem;
use App\Payslip;
use App\SiteAllocation;
use App\Sites;
use Illuminate\Console\Command;

class GenerateInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iguard:invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invoice Generator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Filter Companies Which Have PayDay today
        $companies = Company::where(['invoice_day' => date('d')])->get();

        foreach ($companies as $company){
            //Generate pay day last month to this month
            $datefrom = date('Y-m', strtotime('last month')) . '-' . $company->invoice_day;
            $dateto = date('Y-m') . '-' . $company->invoice_day;

            $sites = Sites::where('company_id',$company->id)->get();
            foreach ($sites as $site){
                $siteAllocations = SiteAllocation::where(['site_id' => $site->id])->get();

                $invoice = new Invoice();
                $invoice->site_id = $site->id;
                $invoice->start_at = $datefrom;
                $invoice->end_at = $dateto;
                $invoice->save();
                $invoiceTotal = 0.00;
                foreach ($siteAllocations as $allocation){
                    $invoice->client_id = $allocation->client_id;
                    $invoice->save();
                    $present = EmployeeAttendance::where(['site_id' => $site->id])
                        ->whereBetween('date',array($datefrom,$dateto))
                        ->where('attendance_type','P')
                        ->where('designation_id',$allocation->designation_id)
                        ->get();

                    $workedOnHoliday = EmployeeAttendance::where(
                        ['site_id' => $site->id])
                        ->whereBetween('date',array($datefrom,$dateto))
                        ->where('attendance_type','WOH')
                        ->where('designation_id',$allocation->designation_id)
                        ->get();

                    $companyHoliday = EmployeeAttendance::where(
                        ['site_id' => $site->id])
                        ->whereBetween('date',array($datefrom,$dateto))
                        ->where('attendance_type','CH')
                        ->where('designation_id',$allocation->designation_id)
                        ->get();

                    $payDays = count($present)
                        + count($companyHoliday)
                        + (2 * count($workedOnHoliday));

                    $subTotal = $payDays * $allocation->rate;
                    $invoiceTotal = $invoiceTotal + $subTotal;
                    InvoiceItem::create([
                        'invoice_id' => $invoice->id,
                        'rate' => $allocation->rate,
                        'particular' => $allocation->designation()->desig_dname,
                        'worked_days' => $payDays,
                        'sub_total' => $subTotal
                    ]);
                }

                $invoice->invoice_amount = $invoiceTotal;
                $invoice->save();

                $this->info('Invoice Generated for : ' . $site->id . ' pay days : '. $invoice->invoice_amount);
            }
        }
    }
}
