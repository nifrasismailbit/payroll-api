<?php

namespace App\Console\Commands;

use App\Company;
use App\Employee;
use App\EmployeeAttendance;
use App\EmployeeLeave;
use App\EmployeeSalary;
use App\Payslip;
use Illuminate\Console\Command;

class GeneratePayslip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iguard:payslip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generating Payslip';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Filter Companies Which Have PayDay today
        $companies = Company::where(['pay_day' => date('d')])->get();

        foreach ($companies as $company){
            //Generate pay day last month to this month
            $datefrom = date('Y-m', strtotime('last month')) . '-' . $company->pay_day;
            $dateto = date('Y-m') . '-' . $company->pay_day;

            $yearStart = date('Y-m-d', strtotime('first day of january this year'));
            $yearEnd = date('Y-m-d', strtotime('last day of december this year'));

            $allowedCasualCount = $company->casual_leaves;

            $employees = Employee::where('company_id',$company->id)->get();
            foreach ($employees as $employee){
                $present = EmployeeAttendance::where(
                    ['employee_id' => $employee->id])
                    ->whereBetween('date',array($datefrom,$dateto))
                    ->where('attendance_type','P')->get();

                $workedOnHoliday = EmployeeAttendance::where(
                    ['employee_id' => $employee->id])
                    ->whereBetween('date',array($datefrom,$dateto))
                    ->where('attendance_type','WOH')->get();

                $absent = EmployeeAttendance::where(
                    ['employee_id' => $employee->id])
                    ->whereBetween('date',array($datefrom,$dateto))
                    ->where('attendance_type','A')->get();

                $companyHoliday = EmployeeAttendance::where(
                    ['employee_id' => $employee->id])
                    ->whereBetween('date',array($datefrom,$dateto))
                    ->where('attendance_type','CH')->get();



                $payDays = count($present) + count($companyHoliday) + (2 * count($workedOnHoliday));

                $leavesInCurrentYear = EmployeeAttendance::where(
                    ['employee_id' => $employee->id])
                    ->whereBetween('date',array($yearStart,$yearEnd))
                    ->get();

                $alreadyTookLeaves = count($leavesInCurrentYear) - count($absent);
                $paidLeave = 0;
                if($alreadyTookLeaves < $allowedCasualCount){
                    $availableLeaves = $allowedCasualCount - $alreadyTookLeaves;
                    if($availableLeaves >= count($absent)){
                        $paidLeave  =  count($absent);
                    }else{
                        $paidLeave =  $availableLeaves;
                    }
                }

                $payDays = $payDays + $paidLeave;

                $calculatedSalary = 0.00;

                $salary = EmployeeSalary::where(['employee_id' => $employee->id])->first();
                $perDayRate = (
                    $salary->basic
                    + $salary->hra
                    + $salary->special_allowance
                    + $salary->uniform_allowance + $salary->leave_travel_allowance )
                    / $company->no_of_working_days;

                $daySalary = $perDayRate * $payDays;

                $calculatedSalary = $daySalary - $salary->pf_employer;

                //TODO Professional Tax Deduction
                $professionalTaxDeduction = 0.00;
                $calculatedSalary = $calculatedSalary - $professionalTaxDeduction;

                $tds = 0.00;
                $calculatedSalary = $calculatedSalary - $tds;

                //TODO add arreares & minus deductions

                Payslip::create([
                    'company_id' => $company->id,
                    'employee_id' => $employee->id,
                    'start_at' => $datefrom,
                    'end_at' => $dateto,
                    'pay_days' =>  $payDays,
                    'present_days' =>  count($present),
                    'absent_days' =>  count($absent),
                    'worked_on_holiday_days' =>  count($workedOnHoliday),
                    'paid_leave_days' =>  $paidLeave,
                    'basic' => $salary->basic,
                    'hra' => $salary->hra,
                    'special_allowance' => $salary->special_allowance,
                    'uniform_allowance' => $salary->uniform_allowance,
                    'leave_travel_allowance' => $salary->leave_travel_allowance,
                    'pf_employer' => $salary->pf_employer,
                    'professional_tax_amount' => $professionalTaxDeduction,
                    'tds_amount' => $tds,
                    'deductions' => 0.00,
                    'arrears' => 0.00,
                    'guaranteed_salary' => $calculatedSalary,
                    'notes' => 'Deductions 0.00'
                ]);

                $this->info('Employee ID : ' . $employee->id . ' pay days : '. $payDays);
            }
        }

    }
}
