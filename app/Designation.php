<?php

namespace App;

class Designation extends BaseModel
{
    protected $table = 'desig_mst';

    public function createdBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class, 'id', 'updated_by');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'dept_id');
    }
}
