<?php

namespace App;

class Department extends BaseModel
{
    protected $table = 'dept_mst';

    public function createdBy()
    {
        return $this->hasOne(User::class,'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class,'id', 'updated_by');
    }

    public function company()
    {
        return $this->hasOne(Company::class,'id', 'company_id');
    }

}
