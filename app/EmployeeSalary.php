<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeSalary extends BaseModel
{
    protected $table = 'employee_salary';
}
