<?php

namespace App;

class SiteAllocation extends BaseModel
{
    protected $table = 'site_allocation';

    protected $fillable = ['site_id', 'client_id', 'designation_id', 'qty'];

    public function site(){
        return $this->hasOne(Company::class,'id', 'site_id');
    }

    public function client(){
        return $this->hasOne(Company::class,'id', 'client_id');
    }

    public function designation(){
        return $this->hasOne(Designation::class,'id', 'designation_id');
    }
}
