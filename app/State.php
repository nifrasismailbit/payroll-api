<?php

namespace App;

class State extends BaseModel
{
    protected $table = 'state_mst';


    public function createdBy()
    {
        return $this->hasOne(User::class,'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class,'id', 'updated_by');
    }

}
