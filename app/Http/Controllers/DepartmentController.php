<?php

namespace App\Http\Controllers;

use App\Department;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class DepartmentController extends Controller
{
    private $authUser;

    public function __construct()
    {

    }

    public function create(Request $request)
    {
        try {
            $user = User::find(auth()->user()->getAuthIdentifier());

            $validator = Validator::make($request->all(), [
                'dept_name' => ['required',
                    Rule::unique('dept_mst')->where(function ($query) use ($request,$user) {
                    return $query
                        ->whereDeptName($request->dept_name)
                        ->whereCompanyId($user->company_id);
                }),],
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'false', 'errors' => $validator->errors()], 422);
            }


            $department = new Department();
            $department->dept_name = $request->input('dept_name');
            $department->dept_sname = $request->input('dept_sname');
            $department->dept_dname = $request->input('dept_dname');
            $department->sort_order = "ASC";
            $department->company_id = $user->company_id;
            $department->created_by = auth()->user()->getAuthIdentifier();
            $department->updated_by = auth()->user()->getAuthIdentifier();

            if ($request->input('dept_sname') == null) {
                $department->dept_sname = $request->input('dept_name');
            }

            if ($request->input('dept_dname') == null) {
                $department->dept_dname = $request->input('dept_name');
            }

            $department->save();

            return response(['message' => 'department created successfully','data' => $department], 200);

        } catch (Exception $exception) {
            return response(['status' => 'false', 'message' => $exception->getMessage()], 422);
        }
    }

    public function edit()
    {

    }

    public function list($id)
    {
        try {
            return Department::where('company_id', $id)->get();
        }catch (\Exception $exception){
            return response()->json(['status' => 'false', 'message' => $exception->getMessage()]);
        }
    }

    public function view($id)
    {
        return Department::find($id);
    }
}
