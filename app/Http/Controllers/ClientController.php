<?php

namespace App\Http\Controllers;

use App\Address;
use App\Company;
use App\Contact;
use App\Repositories\UserRepository;
use App\Sites;
use App\User;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'client_code' => ['required', 'unique:company_mst,company_code', 'max:255'],
                'client_name' => ['required'],
                'client_dname' => ['required'],
                'no_of_working_days' => ['required'],
                'address.add_type' => ['required'],
                'address.contact_name' => ['required'],
                'address.contact_tel' => ['required'],
                'address.contact_mobile' => ['required'],
                'address.add_line1' => ['required'],
                'address.city_id' => ['required', 'exists:city_mst,id'],
                'address.state_id' => ['required', 'exists:state_mst,id'],
                'address.country' => ['required'],
                'address.country_code' => ['required'],
                'address.pin' => ['required'],
                'contact.contact_type' => ['required'],
                'contact.salutation' => ['required'],
                'contact.fname' => ['required'],
                'contact.lname' => ['required'],
                'contact.mobile' => ['required'],
                'contact.email' => ['required','unique:contact_mst,email'],
                'contact.address.add_type' => ['required'],
                'contact.address.contact_name' => ['required'],
                'contact.address.contact_tel' => ['required'],
                'contact.address.contact_mobile' => ['required'],
                'contact.address.add_line1' => ['required'],
                'contact.address.city_id' => ['required', 'exists:city_mst,id'],
                'contact.address.state_id' => ['required', 'exists:state_mst,id'],
                'contact.address.country' => ['required'],
                'contact.address.country_code' => ['required'],
                'contact.address.pin' => ['required'],
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'false', 'errors' => $validator->errors()], 422);
            }

            $user = User::find(auth()->user()->getAuthIdentifier());

            $companyAddress = new Address();
            $companyAddress->add_type = $request->input('address.add_type');
            $companyAddress->contact_name = $request->input('address.contact_name');
            $companyAddress->contact_tel = $request->input('address.contact_tel');
            $companyAddress->contact_mobile = $request->input('address.contact_mobile');
            $companyAddress->add_line1 = $request->input('address.add_line1');
            $companyAddress->city_id = $request->input('address.city_id');
            $companyAddress->state_id = $request->input('address.state_id');
            $companyAddress->country = $request->input('address.country');
            $companyAddress->country_code = $request->input('address.country_code');
            $companyAddress->pin = $request->input('address.pin');
            $companyAddress->created_by = auth()->user()->getAuthIdentifier();
            $companyAddress->updated_by = auth()->user()->getAuthIdentifier();


            $contactAddress = new Address();
            $contactAddress->add_type = $request->input('contact.address.add_type');
            $contactAddress->contact_name = $request->input('contact.address.contact_name');
            $contactAddress->contact_tel = $request->input('contact.address.contact_tel');
            $contactAddress->contact_mobile = $request->input('contact.address.contact_mobile');
            $contactAddress->add_line1 = $request->input('contact.address.add_line1');
            $contactAddress->city_id = $request->input('contact.address.city_id');
            $contactAddress->state_id = $request->input('contact.address.state_id');
            $contactAddress->country = $request->input('contact.address.country');
            $contactAddress->country_code = $request->input('contact.address.country_code');
            $contactAddress->pin = $request->input('contact.address.pin');
            $contactAddress->created_by = auth()->user()->getAuthIdentifier();
            $contactAddress->updated_by = auth()->user()->getAuthIdentifier();

            $contact = new Contact();
            $contact->contact_type = $request->input('contact.contact_type');
            $contact->salutation = $request->input('contact.salutation');
            $contact->fname = $request->input('contact.fname');
            $contact->lname = $request->input('contact.lname');
            $contact->mobile = $request->input('contact.mobile');
            $contact->email = $request->input('contact.email');
            $contact->created_by = auth()->user()->getAuthIdentifier();
            $contact->updated_by = auth()->user()->getAuthIdentifier();

            $company = new Company();
            $company->company_code = $request->input('client_code');
            $company->company_name = $request->input('client_name');
            $company->company_dname = $request->input('client_dname');
            $company->company_type = "client";
            $company->parent_company_id = $user->company_id;
            $company->no_of_working_days = $request->input('no_of_working_days');
            $company->created_by = auth()->user()->getAuthIdentifier();
            $company->updated_by = auth()->user()->getAuthIdentifier();

            DB::transaction(function() use (
                $request,
                $companyAddress,
                $contactAddress,
                $contact,$company) {
                $companyAddress->save();
                $contactAddress->save();

                $contact->address_id = $contactAddress->id;
                $contact->save();

                $company->company_address_id = $companyAddress->id;
                $company->company_contact_id = $contact->id;
                $company->save();

                $data = array();
                $data['email'] = $request->input('contact.email');
                $data['fname'] = $request->input('contact.fname');
                $data['lname'] = $request->input('contact.lname');
                $data['mobile'] = $request->input('contact.mobile');
                $this->userRepository->registerUser($data, 'client-super-admin', $company->id);

            });

            return response(['message' => 'client company is created successfully'], 200);

        } catch (Exception $exception) {
            return response(['status' => 'false', 'message' => $exception->getMessage()], 422);
        }
    }

    public function edit(Request $request)
    {

    }

    public function deactivate(Request $request)
    {

    }

    public function list()
    {
        return Company::with(['address', 'contact', 'contact.address','parentCompany'])->where('company_type','client')->get();
    }

    public function view($id)
    {
        return Company::with(['address', 'contact', 'contact.address','parentCompany'])->find($id);
    }

    public function clientSuperAdminActivation($code, Request $request)
    {
        $user = User::firstWhere('email', $request->input('email'));
        if ($user) {
            if ($user->activation_code === $code) {
                $user->password = $request->input('password');
                $user->email_verified_at = new DateTime();
                $user->activation_code = "";
                $user->save();
                return response(['message' => 'you account has been verified sucessfully'], 200);
            }
        }
        return response(['message' => 'verification failed'], 403);
    }

}
