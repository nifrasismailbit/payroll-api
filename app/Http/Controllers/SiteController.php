<?php

namespace App\Http\Controllers;

use App\Address;
use App\Company;
use App\Contact;
use App\Invoice;
use App\Payslip;
use App\Repositories\UserRepository;
use App\SiteAllocation;
use App\Sites;
use App\User;
use DateTime;
use Exception;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SiteController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'site_code' => ['required', 'unique:company_mst,company_code', 'max:255'],
                'site_name' => ['required'],
                'site_dname' => ['required'],
                'client_id' => ['required'],
                'company_id' => ['required'],
                'invoice_day' => ['required'],
                'no_of_working_days' => ['required'],
                'address.add_type' => ['required'],
                'address.contact_name' => ['required'],
                'address.contact_tel' => ['required'],
                'address.contact_mobile' => ['required'],
                'address.add_line1' => ['required'],
                'address.city_id' => ['required', 'exists:city_mst,id'],
                'address.state_id' => ['required', 'exists:state_mst,id'],
                'address.country' => ['required'],
                'address.country_code' => ['required'],
                'address.pin' => ['required'],
                'contact.contact_type' => ['required'],
                'contact.salutation' => ['required'],
                'contact.fname' => ['required'],
                'contact.lname' => ['required'],
                'contact.mobile' => ['required'],
                'contact.email' => ['required','unique:contact_mst,email'],
                'contact.address.add_type' => ['required'],
                'contact.address.contact_name' => ['required'],
                'contact.address.contact_tel' => ['required'],
                'contact.address.contact_mobile' => ['required'],
                'contact.address.add_line1' => ['required'],
                'contact.address.city_id' => ['required', 'exists:city_mst,id'],
                'contact.address.state_id' => ['required', 'exists:state_mst,id'],
                'contact.address.country' => ['required'],
                'contact.address.country_code' => ['required'],
                'contact.address.pin' => ['required'],
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'false', 'errors' => $validator->errors()], 422);
            }

            $user = User::find(auth()->user()->getAuthIdentifier());

            $companyAddress = new Address();
            $companyAddress->add_type = $request->input('address.add_type');
            $companyAddress->contact_name = $request->input('address.contact_name');
            $companyAddress->contact_tel = $request->input('address.contact_tel');
            $companyAddress->contact_mobile = $request->input('address.contact_mobile');
            $companyAddress->add_line1 = $request->input('address.add_line1');
            $companyAddress->city_id = $request->input('address.city_id');
            $companyAddress->state_id = $request->input('address.state_id');
            $companyAddress->country = $request->input('address.country');
            $companyAddress->country_code = $request->input('address.country_code');
            $companyAddress->pin = $request->input('address.pin');
            $companyAddress->created_by = auth()->user()->getAuthIdentifier();
            $companyAddress->updated_by = auth()->user()->getAuthIdentifier();


            $contactAddress = new Address();
            $contactAddress->add_type = $request->input('contact.address.add_type');
            $contactAddress->contact_name = $request->input('contact.address.contact_name');
            $contactAddress->contact_tel = $request->input('contact.address.contact_tel');
            $contactAddress->contact_mobile = $request->input('contact.address.contact_mobile');
            $contactAddress->add_line1 = $request->input('contact.address.add_line1');
            $contactAddress->city_id = $request->input('contact.address.city_id');
            $contactAddress->state_id = $request->input('contact.address.state_id');
            $contactAddress->country = $request->input('contact.address.country');
            $contactAddress->country_code = $request->input('contact.address.country_code');
            $contactAddress->pin = $request->input('contact.address.pin');
            $contactAddress->created_by = auth()->user()->getAuthIdentifier();
            $contactAddress->updated_by = auth()->user()->getAuthIdentifier();

            $contact = new Contact();
            $contact->contact_type = $request->input('contact.contact_type');
            $contact->salutation = $request->input('contact.salutation');
            $contact->fname = $request->input('contact.fname');
            $contact->lname = $request->input('contact.lname');
            $contact->mobile = $request->input('contact.mobile');
            $contact->email = $request->input('contact.email');
            $contact->created_by = auth()->user()->getAuthIdentifier();
            $contact->updated_by = auth()->user()->getAuthIdentifier();

            $company = new Company();
            $company->company_code = $request->input('site_code');
            $company->company_name = $request->input('site_name');
            $company->company_dname = $request->input('site_dname');
            $company->invoice_day = $request->input('invoice_day');
            $company->company_type = "site";
            $company->parent_company_id = $user->company_id;
            $company->no_of_working_days = $request->input('no_of_working_days');
            $company->created_by = auth()->user()->getAuthIdentifier();
            $company->updated_by = auth()->user()->getAuthIdentifier();

            DB::transaction(function() use (
                $request,
                $companyAddress,
                $contactAddress,
                $contact,$company) {
                $companyAddress->save();
                $contactAddress->save();

                $contact->address_id = $contactAddress->id;
                $contact->save();

                $company->company_address_id = $companyAddress->id;
                $company->company_contact_id = $contact->id;
                $company->save();

                Sites::create([
                    'site_id' => $company->id,
                    'client_id' => $request->input('client_id'),
                    'company_id' => $request->input('company_id')
                ]);

                $data = array();
                $data['email'] = $request->input('contact.email');
                $data['fname'] = $request->input('contact.fname');
                $data['lname'] = $request->input('contact.lname');
                $data['mobile'] = $request->input('contact.mobile');
                $this->userRepository->registerUser($data, 'site-super-admin', $company->id);

            });

            return response(['message' => 'site company is created successfully'], 200);

        } catch (Exception $exception) {
            return response(['status' => 'false', 'message' => $exception->getMessage()], 422);
        }
    }

    public function edit(Request $request)
    {

    }

    public function deactivate(Request $request)
    {

    }

    public function list()
    {
        return Company::with(['address', 'contact', 'contact.address','parentCompany'])->where('company_type','site')->get();
    }

    public function view($id)
    {
        return Company::with(['address', 'contact', 'contact.address','parentCompany'])->find($id);
    }

    public function siteSuperAdminActivation($code, Request $request)
    {
        $user = User::firstWhere('email', $request->input('email'));
        if ($user) {
            if ($user->activation_code === $code) {
                $user->password = $request->input('password');
                $user->email_verified_at = new DateTime();
                $user->activation_code = "";
                $user->save();
                return response(['message' => 'you account has been verified sucessfully'], 200);
            }
        }
        return response(['message' => 'verification failed'], 403);
    }

    public function siteAllocationRequest(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'site_id' => ['required'],
                'client_id' => ['required'],
                'designation_id' => ['required'],
                'qty' => ['required'],
                ]);

            SiteAllocation::create([
                'site_id' => $request->input('site_id'),
                'client_id' => $request->input('client_id'),
                'designation_id' => $request->input('designation_id'),
                'qty' => $request->input('qty')
            ]);
            return response()->json(['message' => 'site allocation request has been sent']);
        }catch (Exception $exception){
            return response()->json(['status' => 'false', 'message' => $exception->getMessage()],422);
        }
    }

    public function siteAllocationStatusUpdate(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'site_allocation_id' => ['required'],
                'status' => ['required'],
                'rate' => ['required'],
            ]);


            $siteAllocRequest = SiteAllocation::find($request->input('site_allocation_id'));
            $siteAllocRequest->rate = $request->input('rate');
            $siteAllocRequest->status = $request->input('status');
            $siteAllocRequest->save();
            return response()->json(['message' => 'site allocation request has been updated']);
        }catch (Exception $exception){
            return response()->json(['status' => 'false', 'message' => $exception->getMessage()],422);
        }
    }

    public function allocationListByCompany(){
        $user = User::find(auth()->user()->getAuthIdentifier());
        $sites = Sites::where(['company_id' => $user->company_id])->get();
        $allocations = array();
        foreach ($sites as $site){

            $siteAllocations = SiteAllocation::with('site','client','designation')->where(['site_id' => $site->site_id])->get();
            foreach ($siteAllocations as $alloc){
                array_push($allocations,$alloc);
            }
        }

        return response()->json(['data' => $allocations]);
    }

    public function siteListByCompany($companyId){
        return Sites::with('site','client','company')->where('company_id',$companyId)->get();
    }

    public function siteListByClient($clientId){
        return Sites::with('site','client','company')->where('client_id',$clientId)->get();
    }

    public function listAllInvoices(Request $request){
        try{
            if($request->input('client_id') && $request->input('employee_id')){
                $invoices = Invoice::with('invoiceItems')->where(['client_id' => $request->input('client_id'), 'site_id' => $request->input('site_id')])->get();
            }elseif ($request->input('client_id')){
                $invoices = Invoice::where(['client_id' => $request->input('client_id')])->get();
            }
            return response()->json(['data' => $invoices]);
        }catch (\Exception $exception){
            return \response()->json(['status' => 'false', 'message' => $exception->getMessage()]);
        }
    }

    public function invoiceView($id){
        try{
            $invoice = Invoice::find($id);
            return response()->json(['data' => $invoice]);
        }catch (\Exception $exception){
            return \response()->json(['status' => 'false', 'message' => $exception->getMessage()]);
        }
    }


}
