<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;
use App\EmployeeAddress;
use App\EmployeeBankInfo;
use App\EmployeeSalary;
use App\Exceptions\CustomException;
use App\Imports\EmployeeAttendanceImport;
use App\Imports\EmployeeImport;
use App\Payslip;
use App\User;
use http\Env\Response;
use Illuminate\Database\Connection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller
{
    private $authUser;

    public function __construct()
    {
       // $this->authUser = User::find(auth()->user()->getAuthIdentifier());
    }

    public function create(Request $request)
    {
        try {
            $this->authUser = User::find(auth()->user()->getAuthIdentifier());
            $validator = Validator::make($request->all(), [
                'title' => ['required'],
                'fname' => ['required'],
                'lname' => ['required'],
                'gender' => ['required'],
                'marital_status' => ['required'],
                'personal_email' => ['required'],
                'mobile_no' => ['required'],
                'pan_number' => ['required'],
                'pf_number' => ['required'],
                'esic_number' => ['required'],
                'voter_id' => ['required'],
                'uan_number' => ['required'],
                'aadhar_number' => ['required'],
                'national_id_number' => ['required'],
                'blood_group' => ['required'],
                'employee_code' => ['required'],
                'official_email_id' => ['required'],
                'designation_id' => ['required','exists:desig_mst,id'],
                'grade' => ['required'],
                'joining_date' => ['required'],
                'salary.band' => ['required'],
                'salary.basic' => ['required'],
            'salary.hra' => ['required'],
            'salary.special_allowance' => ['required'],
            'salary.uniform_allowance' => ['required'],
            'salary.leave_travel_allowance' => ['required'],
            'salary.pf_employer' => ['required'],
            'salary.professional_tax' => ['required'],
            'salary.tds' => ['required'],
                'bank.bank_name' => ['required'],
                'bank.ifsc_code' => ['required'],
                'bank.account_type' => ['required'],
                'bank.bank_account_number' => ['required'],
                'bank.branch_name' => ['required'],
                'bank.branch_address' => ['required'],
                'bank.account_holder_name' => ['required'],
                'address.address_type' => ['required'],
                'address.address' => ['required'],
                'address.city_id' => ['required'],
                'address.state_id' => ['required'],
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'false', 'errors' => $validator->errors()], 422);
            }

            $employee = new Employee();
            $employee->title = $request->input('title');
            $employee->fname = $request->input('fname');
            $employee->mname = $request->input('mname');
            $employee->lname = $request->input('lname');
            $employee->gender = $request->input('gender');
            $employee->marital_status = $request->input('marital_status');
            $employee->personal_email = $request->input('personal_email');
            $employee->mobile_no = $request->input('mobile_no');
            $employee->pan_number = $request->input('pan_number');
            $employee->pf_number = $request->input('pf_number');
            $employee->esic_number = $request->input('esic_number');
            $employee->voter_id = $request->input('voter_id');
            $employee->uan_number = $request->input('uan_number');
            $employee->aadhar_number = $request->input('aadhar_number');
            $employee->national_id_number = $request->input('national_id_number');
            $employee->blood_group = $request->input('blood_group');
            $employee->employee_code = $request->input('employee_code');
            $employee->official_email_id = $request->input('official_email_id');
            $employee->designation_id = $request->input('designation_id');
            $employee->l1_manager_id = $request->input('l1_manager_id');
            $employee->l2_manager_id = $request->input('l2_manager_id');
            $employee->grade = $request->input('grade');
            $employee->joining_date = $request->input('joining_date');
            $employee->hr_manager_id = $request->input('hr_manager_id');
            $employee->company_id = $this->authUser->company_id;
            $employee->created_by = auth()->user()->getAuthIdentifier();
            $employee->updated_by = auth()->user()->getAuthIdentifier();

            $employeeSalary = new EmployeeSalary;
            $employeeSalary->currency = 'INR';
            $employeeSalary->band = $request->input('salary.band');
            $employeeSalary->basic = $request->input('salary.basic');
            $employeeSalary->hra = $request->input('salary.hra');
            $employeeSalary->special_allowance = $request->input('salary.special_allowance');
            $employeeSalary->uniform_allowance = $request->input('salary.uniform_allowance');
            $employeeSalary->leave_travel_allowance = $request->input('salary.leave_travel_allowance');
            $employeeSalary->pf_employer = $request->input('salary.pf_employer');
            $employeeSalary->professional_tax = $request->input('salary.professional_tax');
            $employeeSalary->tds = $request->input('salary.tds');
            $employeeSalary->created_by = auth()->user()->getAuthIdentifier();
            $employeeSalary->updated_by = auth()->user()->getAuthIdentifier();

            $employeeBankInfo = new EmployeeBankInfo;
            $employeeBankInfo->bank_name = $request->input('bank.bank_name');
            $employeeBankInfo->ifsc_code = $request->input('bank.ifsc_code');
            $employeeBankInfo->account_type = $request->input('bank.account_type');
            $employeeBankInfo->bank_account_number = $request->input('bank.bank_account_number');
            $employeeBankInfo->branch_name = $request->input('bank.branch_name');
            $employeeBankInfo->account_holder_name = $request->input('bank.account_holder_name');
            $employeeBankInfo->branch_address = $request->input('bank.branch_address');
            $employeeBankInfo->created_by = auth()->user()->getAuthIdentifier();
            $employeeBankInfo->updated_by = auth()->user()->getAuthIdentifier();

            $employeeAddress = new EmployeeAddress;
            $employeeAddress->address_type = $request->input('address.address_type');
            $employeeAddress->address_type = $request->input('address.address_type');
            $employeeAddress->address = $request->input('address.address');
            $employeeAddress->city_id = $request->input('address.city_id');
            $employeeAddress->state_id = $request->input('address.state_id');
            $employeeAddress->created_by = auth()->user()->getAuthIdentifier();
            $employeeAddress->updated_by = auth()->user()->getAuthIdentifier();


            DB::transaction(function() use ($employee, $employeeSalary,$employeeBankInfo,$employeeAddress) {

                $employee->save();

                $employeeSalary->employee_id = $employee->id;
                $employeeSalary->save();

                $employeeBankInfo->employee_id = $employee->id;
                $employeeBankInfo->save();

                $employeeAddress->employee_id = $employee->id;
                $employeeAddress->save();

            });

            return response(['message' => 'employee basic information saved successfully'], 200);

        } catch (Exception $exception) {
            return response(['status' => 'false', 'message' => $exception->getMessage()], 422);
        }
    }

    public function list($companyId){
        $this->authUser = User::find(auth()->user()->getAuthIdentifier());
        return Employee::with('company','designation')->where('company_id',$this->authUser->company_id)->get();
    }

    public function view($employeeId){
        return Employee::find($employeeId)->with('company','designation')->first();
    }

    public function import(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'company_id' => ['required','exists:company_mst,id'],
                'file' => ['required'],
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'false', 'errors' => $validator->errors()], 422);
            }

            Excel::import(new EmployeeImport($request->input('company_id')),request()->file('file'));
            return response()->json(['message' => 'employee has been imported successfully']);

        }catch (CustomException $exception){
            return response()->json(['status' => 'false', 'message' => 'invalid employee sheet', 'data' => $exception->getCustomMessage()],422);
        }
    }

    public function listAllPayslips(Request $request){
        try{
            if($request->input('company_id') && $request->input('employee_id')){
                $payslips = Payslip::where(['company_id' => $request->input('company_id'), 'employee_id' => $request->input('employee_id')])->get();
            }elseif ($request->input('company_id')){
                $payslips = Payslip::where(['company_id' => $request->input('company_id')])->get();
            }
            return response()->json(['data' => $payslips]);
        }catch (\Exception $exception){
            return \response()->json(['status' => 'false', 'message' => $exception->getMessage()]);
        }
    }

    public function payslipView($id){
        try{
            $payslip = Payslip::find($id);
            return response()->json(['data' => $payslip]);
        }catch (\Exception $exception){
            return \response()->json(['status' => 'false', 'message' => $exception->getMessage()]);
        }
    }
}
