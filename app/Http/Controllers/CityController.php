<?php

namespace App\Http\Controllers;

use App\City;
use App\Department;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Exception;

class CityController extends Controller
{
    public function create(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'name' => ['required','unique:city_mst'],
                'state_id' => ['required','exists:state_mst,id'],
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'false', 'errors' => $validator->errors()], 422);
            }

            $city = new City();
            $city->name = $request->input('name');
            $city->state_id = $request->input('state_id');
            $city->sort_order = 0;
            $city->active = 1;
            $city->created_by = auth()->user()->getAuthIdentifier();
            $city->updated_by = auth()->user()->getAuthIdentifier();
            $city->save();
            return response(['message' => 'city created successfully'], 200);

        } catch (Exception $exception) {
            return response(['status' => 'false', 'message' => $exception->getMessage()], 422);
        }
    }

    public function list(){
        return City::with('state')->get();
    }

    public function listByState($key){
        return City::where('state_id',$key)->with('state')->get();
    }

    public function view($id){
        return City::find($id)->with('state')->get();
    }
}
