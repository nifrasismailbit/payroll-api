<?php

namespace App\Http\Controllers;

use App\Designation;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DesignationController extends Controller
{
    private $authUser;

    public function __construct()
    {
        $this->authUser = User::find(auth()->user()->getAuthIdentifier());
    }

    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'desig_name' => ['required', 'unique:desig_mst,desig_name'],
                'dept_id' => ['required', 'exists:dept_mst,id'],
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'false', 'errors' => $validator->errors()], 422);
            }

            $designation = new Designation();
            $designation->desig_name = $request->input('desig_name');
            $designation->desig_sname = $request->input('desig_sname');
            $designation->desig_dname = $request->input('desig_dname');
            $designation->sort_order = "ASC";
            $designation->company_id = $this->authUser->company_id;
            $designation->dept_id = $request->input('dept_id');
            $designation->created_by = auth()->user()->getAuthIdentifier();
            $designation->updated_by = auth()->user()->getAuthIdentifier();

            if ($request->input('desig_sname') == null) {
                $designation->desig_sname = $request->input('desig_name');
            }

            if ($request->input('desig_dname') == null) {
                $designation->desig_dname = $request->input('desig_name');
            }

            $designation->save();

            return response(['message' => 'designation created successfully', 'data' => $designation], 200);

        } catch (Exception $exception) {
            return response(['status' => 'false', 'message' => $exception->getMessage()], 422);
        }
    }

    public function edit()
    {

    }

    public function list($id)
    {
        try {
            return Designation::where('company_id', $id)->get();
        } catch (Exception $exception) {
            return response()->json(['status' => 'false', 'message' => $exception->getMessage()], 403);
        }
    }

    public function view($id)
    {
        return Designation::find($id);
    }
}
