<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeAttendance;
use App\EmployeeLeave;
use App\Exceptions\CustomException;
use App\Imports\EmployeeAttendanceImport;
use App\Imports\PublicHolidayCompanyImport;
use App\PublicHolidayCompany;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;

class ImportEmployeeAttendanceController extends Controller
{
    public function import(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'site_id' => ['required'],
                'file' => ['required'],
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'false', 'errors' => $validator->errors()], 422);
            }

            $user = User::find(auth()->user()->getAuthIdentifier());
            //PublicHolidayCompany::where('company_id',$user->company_id)->delete();

            $headings = (new HeadingRowImport)->toArray(request()->file('file'));
            Excel::import(new EmployeeAttendanceImport($headings,$request->input('site_id')),request()->file('file'));
            return response()->json(['message' => 'employee attendance imported successfully']);

        }catch (CustomException $exception){
            return response()->json(['status' => 'false', 'message' => 'invalid attendance sheet', 'data' => $exception->getCustomMessage()],422);
        }
    }

    public function list($startDate, $endDate){
        try{
            $employees = Employee::all();
            $data = array();
            foreach ($employees as $employee){
                $attendance = EmployeeAttendance
                    ::whereBetween('date',[$startDate,$endDate])
                    ->where('employee_id',$employee->id)
                    ->get()->sortBy('date');
                array_push($data,['employee' => $employee, 'attendance' => $attendance]);
            }
            return response()->json(['data' => $data],200);
        }catch (\Exception $exception){
            return response()->json(['status' => 'false', 'message' => $exception->getMessage()],422);
        }
    }

    public function listOfLeaves(){
        try{
            $user = User::find(auth()->user()->getAuthIdentifier());
            $data = EmployeeLeave::where(['company_id' => $user->company_id])->get();;
            return response()->json(['data' => $data],200);
        }catch (\Exception $exception){
            return response()->json(['status' => 'false', 'message' => $exception->getMessage()],422);
        }
    }
}
