<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $grant = false;
        if($request->role == 'company-super-admin'){
            if(auth()->user()->can('add-company-super-admin')){
                $grant = true;
            };
        }elseif ($request->role == 'client-super-admin'){
            if(auth()->user()->can('add-client-super-admin')){
                $grant = true;
            };
        }elseif ($request->role == 'site-super-admin'){
            if(auth()->user()->can('add-site-super-admin')){
                $grant = true;
            };
        }elseif ($request->role == 'employee'){
            if(auth()->user()->can('add-employee')){
                $grant = true;
            };
        }

        if($grant) {
            $user = User::create([
                'fname' => $request->fname,
                'lname' => $request->lname,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'country_code' => $request->country_code,
                'level' => $request->level,
                'password' => $request->password,

            ]);

            $role = Role::findByName($request->role);
            if ($role) {
                $user->assignRole($role);
            }

            $token = auth()->login($user);

            return $this->respondWithToken($token);
        }
        return false;
    }

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = User::firstWhere('email',$credentials['email']);
        if($user->email_verified_at != null){
            return $this->respondWithToken($token);
        }else{
            return response()->json(['status' => 'false', 'message' => 'you account is not verified yet']);
        }

    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token)
    {
        $user = User::find(auth()->user()->getAuthIdentifier());
        $company = Company::find($user->company_id);
        if($company) {
            $parentCompany = Company::find($company->parent_company_id);
        }else{
            $parentCompany = null;
        }
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60,
            'user' => auth()->user(),
            'role' => auth()->user()->roles,
            'parent_company' => $parentCompany
        ]);
    }

    public function view(){
        return response()->json([
            'user' => auth()->user(),
            'role' => auth()->user()->roles
        ]);
    }
}
