<?php

namespace App\Http\Controllers;

use App\Imports\PublicHolidayCompanyImport;
use App\PublicHolidayCompany;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportCompanyHolidayController extends Controller
{
    public function import()
    {
        $user = User::find(auth()->user()->getAuthIdentifier());
        PublicHolidayCompany::where('company_id',$user->company_id)->delete();

        Excel::import(new PublicHolidayCompanyImport(),request()->file('file'));
        return response()->json(['message' => 'company holiday sheet import success']);
    }
}
