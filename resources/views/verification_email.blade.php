<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8" />
</head>
<body>
<p>Hi, {{ $name}}</p>
<p>Please activate the account using the following activation code</p>
<p>Your activation key is {{ $activation_key }}</p>
</body>
</html>