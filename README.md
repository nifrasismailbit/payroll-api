# iGuards API v1

# Setup API

- Setup Database on .env

- Migrate the dataase and create tables
```shell script
php artisan migrate:fresh
php artisan db:seed
php artisan jwt:secret
```

- Generating App Super Admin in tinker

```shell script
php artisan tinker
```

```shell script
$role = \Spatie\Permission\Models\Role::findByName('app-super-admin');
$user = \App\User::create(['email' => 'admin@iguard.in','password' => 'admin', 'fname' => 'nifras', 'lname' => 'ismail', 'mobile' => '0778990300', 'country_code' => "IN",'level' => 'M1','email_verified_at' => '2020-05-17 11:06:16']);
$user->assignRole($role);
```

- Start the development server 
```shell script
  php artisan serve
```

## API Documentation ( Progressive Document )

https://www.getpostman.com/collections/289ab9dde9f0cf7b4a4d

## Environment Setup 

https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose