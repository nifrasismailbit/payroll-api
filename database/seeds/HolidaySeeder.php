<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HolidaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path('app/public_holiday.csv');
        $handle = fopen($path, 'r');
        $header = false;

        DB::table('public_holiday_mst')->delete();

        while ($csvLine = fgetcsv($handle, 1000, ",")) {

            if ($header) {
                $header = true;
            } else {
                $publicHoliday = new \App\PublicHoliday();
                $publicHoliday->date = trim($csvLine[1]);
                $publicHoliday->label = trim($csvLine[2]);
                $publicHoliday->country = trim($csvLine[3]);
                $publicHoliday->save();
            }
        }
    }
}
