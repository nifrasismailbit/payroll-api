<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path('app/states.csv');
        $handle = fopen($path, 'r');
        $header = false;

        DB::table('state_mst')->delete();

        while ($csvLine = fgetcsv($handle, 1000, ",")) {

            if ($header) {
                $header = true;
            } else {
                $state = new \App\State();
                $state->name = trim($csvLine[1]);
                $state->dname = trim($csvLine[1]);
                $state->gst_code = trim($csvLine[4]);
                $state->sort_order = 0;
                $state->save();
            }
        }
    }
}
