<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // permissions for app-super-admin
        Permission::create(['name' => 'add-companies']);
        Permission::create(['name' => 'view-companies']);
        Permission::create(['name' => 'edit-companies']);
        Permission::create(['name' => 'activate-companies']);
        Permission::create(['name' => 'deactivate-companies']);
        Permission::create(['name' => 'view-users']);
        Permission::create(['name' => 'deactivate-users']);
        Permission::create(['name' => 'reset-users-password']);
        Permission::create(['name' => 'add-company-super-admin']);
        Permission::create(['name' => 'add-client-super-admin']);

        // permissions for company-super-admin
        Permission::create(['name' => 'add-departments']);
        Permission::create(['name' => 'edit-departments']);
        Permission::create(['name' => 'add-designations']);
        Permission::create(['name' => 'edit-designations']);
        Permission::create(['name' => 'add-employees']);
        Permission::create(['name' => 'edit-employees']);
        Permission::create(['name' => 'add-employees-batch']);
        Permission::create(['name' => 'add-site']);
        Permission::create(['name' => 'edit-site']);
        Permission::create(['name' => 'approve-contracts']);
        Permission::create(['name' => 'add-site-super-admin']);

        // permissions for client-super-admin
        // all permissions of site-super-admin plus below
        Permission::create(['name' => 'pay-invoices']);
        Permission::create(['name' => 'request-contracts']);
        Permission::create(['name' => 'view-invoices']);
        Permission::create(['name' => 'view-contracts']);

        // permissions for site-super-admin
        Permission::create(['name' => 'mark-attendance']);
        Permission::create(['name' => 'generate-invoice']);
        Permission::create(['name' => 'view-employees']);
        Permission::create(['name' => 'generate-payslips']);

        // permissions for employee
        Permission::create(['name' => 'view-my-profile']);
        Permission::create(['name' => 'view-payslips']);
        Permission::create(['name' => 'add-employee']);

        $roleSiteSuperAdmin = Role::create(['name' => 'site-super-admin']);
        $roleSiteSuperAdmin->givePermissionTo('mark-attendance');
        $roleSiteSuperAdmin->givePermissionTo('generate-invoice');
        $roleSiteSuperAdmin->givePermissionTo('view-employees');
        $roleSiteSuperAdmin->givePermissionTo('generate-payslips');

        $roleClientSuperAdmin = Role::create(['name' => 'client-super-admin']);
        $roleClientSuperAdmin->givePermissionTo('pay-invoices');
        $roleClientSuperAdmin->givePermissionTo('request-contracts');
        $roleClientSuperAdmin->givePermissionTo('view-invoices');
        $roleClientSuperAdmin->givePermissionTo('generate-payslips');
        $roleClientSuperAdmin->givePermissionTo('mark-attendance');
        $roleClientSuperAdmin->givePermissionTo('generate-invoice');
        $roleClientSuperAdmin->givePermissionTo('view-employees');
        $roleClientSuperAdmin->givePermissionTo('generate-payslips');

        $roleEmployee = Role::create(['name' => 'employee']);
        $roleEmployee->givePermissionTo('view-my-profile');
        $roleEmployee->givePermissionTo('view-payslips');

        $roleCompanySuperAdmin = Role::create(['name' => 'company-super-admin']);
        $roleCompanySuperAdmin->givePermissionTo('add-departments');
        $roleCompanySuperAdmin->givePermissionTo('edit-departments');
        $roleCompanySuperAdmin->givePermissionTo('add-designations');
        $roleCompanySuperAdmin->givePermissionTo('edit-designations');
        $roleCompanySuperAdmin->givePermissionTo('add-employees');
        $roleCompanySuperAdmin->givePermissionTo('edit-employees');
        $roleCompanySuperAdmin->givePermissionTo('add-employees-batch');
        $roleCompanySuperAdmin->givePermissionTo('add-site');
        $roleCompanySuperAdmin->givePermissionTo('edit-site');
        $roleCompanySuperAdmin->givePermissionTo('approve-contracts');
        $roleCompanySuperAdmin->givePermissionTo('add-client-super-admin');
        $roleCompanySuperAdmin->givePermissionTo('add-employee');
        $roleCompanySuperAdmin->givePermissionTo('add-site-super-admin');

        $roleAppSuperAdmin = Role::create(['name' => 'app-super-admin']);
        $roleAppSuperAdmin->givePermissionTo('add-companies');
        $roleAppSuperAdmin->givePermissionTo('edit-companies');
        $roleAppSuperAdmin->givePermissionTo('activate-companies');
        $roleAppSuperAdmin->givePermissionTo('deactivate-companies');
        $roleAppSuperAdmin->givePermissionTo('add-company-super-admin');

    }
}
