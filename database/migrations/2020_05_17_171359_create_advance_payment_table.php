<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvancePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_payment', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type');
            $table->string('mode_of_deduction');
            $table->double('installment_amount');
            $table->double('settlements');
            $table->double('arrears');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance');
    }
}
