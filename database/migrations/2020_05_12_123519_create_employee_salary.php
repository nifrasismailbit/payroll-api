<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salary', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('currency');
            $table->string('band');
            $table->double('basic');
            $table->double('hra')->default(0.00);
            $table->double('special_allowance')->default(0.00);
            $table->double('uniform_allowance')->default(0.00);
            $table->double('leave_travel_allowance')->default(0.00);
            $table->double('pf_employer')->default(0.00);
            $table->double('professional_tax')->default(0.00);
            $table->double('tds')->default(0.00);
            $table->uuid('employee_id');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('employee_id')->references('id')->on('employee');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_salary');
    }
}
