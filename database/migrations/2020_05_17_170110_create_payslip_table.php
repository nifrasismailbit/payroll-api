<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayslipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslip', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('employee_id');
            $table->uuid('company_id');
            $table->date('start_at');
            $table->date('end_at');
            $table->integer('pay_days');
            $table->integer('present_days')->default(0);
            $table->integer('absent_days')->default(0);
            $table->integer('worked_on_holiday_days')->default(0);
            $table->integer('paid_leave_days')->default(0);
            $table->double('basic');
            $table->double('hra');
            $table->double('special_allowance')->default(0.00);
            $table->double('uniform_allowance')->default(0.00);
            $table->double('leave_travel_allowance')->default(0.00);
            $table->double('pf_employer')->default(0.00);
            $table->double('professional_tax_amount')->default(0.00);
            $table->double('tds_amount')->default(0.00);
            $table->double('deductions')->default(0.00);
            $table->double('arrears')->default(0.00);
            $table->double('guaranteed_salary')->default(0.00);
            $table->text('notes')->nullable();
            $table->foreign('employee_id')->references('id')->on('employee');
            $table->foreign('company_id')->references('id')->on('company_mst');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslip');
    }
}
