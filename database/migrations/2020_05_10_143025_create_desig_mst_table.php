<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesigMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desig_mst', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('desig_name');
            $table->string('desig_sname');
            $table->string('desig_dname');
            $table->string('sort_order')->default(0);
            $table->uuid('company_id');
            $table->uuid('dept_id');
            $table->boolean('active')->default(true);
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->foreign('company_id')->references('id')->on('company_mst');
            $table->foreign('dept_id')->references('id')->on('dept_mst');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desig_mst');
    }
}
