<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_payments', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('mode_of_payment');
            $table->string('bank_name');
            $table->string('branch_name');
            $table->string('acc_holder_name');
            $table->string('acc_number');
            $table->double('amount_paid');
            $table->double('arrears');
            $table->double('payment_type');
            $table->string('transfer_status');
            $table->uuid('payslip_id')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->string('note');
            $table->foreign('payslip_id')->references('id')->on('payslip');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_payments');
    }
}
