<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_mst', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('contact_type');
            $table->string('salutation');
            $table->string('fname');
            $table->string('lname');
            $table->string('mobile');
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('email');
            $table->boolean('active')->default(true);
            $table->uuid('address_id');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->foreign('address_id')->references('id')->on('address_mst');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_mst');
    }
}
