<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentCompanyIdToCompanyMst extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_mst', function (Blueprint $table) {
            $table->uuid('parent_company_id')->nullable();
            $table->foreign('parent_company_id')->references('id')->on('company_mst');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_mst', function (Blueprint $table) {
            $table->dropColumn('parent_company_id');
        });
    }
}
