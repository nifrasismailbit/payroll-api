<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_mst', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('company_code');
            $table->string('company_name');
            $table->string('company_dname');
            $table->string('gst_code')->nullable();
            $table->integer('no_of_working_days');
            $table->integer('pay_day')->nullable();
            $table->integer('casual_leaves')->default(0);
            $table->integer('invoice_day')->nullable();
            $table->boolean('active')->default(true);
            $table->uuid('company_address_id');
            $table->uuid('company_contact_id');
            $table->string('company_type');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('company_address_id')->references('id')->on('address_mst');
            $table->foreign('company_contact_id')->references('id')->on('contact_mst');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_mst');
    }
}
