<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     * payscale types from public_holiday:pay_x
     * normal day pay_x is 1
     * no pay leave pay_x is 0
     * casual leave pay_x is 1
     * @return void
     */
    public function up()
    {
        Schema::create('employee_attendance', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('employee_id');
            $table->date('date');
            $table->double('pay_x');
            $table->string('attendance_type');
            $table->uuid('site_id');
            $table->uuid('designation_id');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->foreign('site_id')->references('id')->on('company_mst');
            $table->foreign('designation_id')->references('id')->on('desig_mst');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_attendance');
    }
}
