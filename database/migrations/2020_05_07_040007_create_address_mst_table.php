<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_mst', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('add_type');
            $table->string('contact_name');
            $table->string('contact_tel');
            $table->string('contact_mobile');
            $table->string('add_line1');
            $table->string('add_line2')->nullable();
            $table->uuid('city_id');
            $table->uuid('state_id');
            $table->string('country');
            $table->string('country_code');
            $table->string('pin');
            $table->double('lat')->nullable();
            $table->double('long')->nullable();
            $table->boolean('active')->default(true);
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('state_id')->references('id')->on('state_mst');
            $table->foreign('city_id')->references('id')->on('city_mst');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_mst');
    }
}
