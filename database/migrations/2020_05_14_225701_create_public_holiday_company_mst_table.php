<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublicHolidayCompanyMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_holiday_company_mst', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type')->default('public_holiday');
            $table->string('date');
            $table->string('label');
            $table->string('country');
            $table->double('pay_x');
            $table->uuid('company_id');
            $table->foreign('company_id')->references('id')->on('company_mst');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public_holiday_company');
    }
}
