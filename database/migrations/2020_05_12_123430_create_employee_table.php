<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title');
            $table->string('fname');
            $table->string('mname')->nullable();
            $table->string('lname');
            $table->string('father_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('birth_place')->nullable();
            $table->string('country_of_birth')->nullable();
            $table->string('gender');
            $table->string('marital_status');
            $table->string('date_of_marriage')->nullable();
            $table->string('personal_email');
            $table->string('mobile_no');
            $table->string('pan_number');
            $table->string('pf_number');
            $table->string('esic_number');
            $table->string('voter_id');
            $table->string('uan_number');
            $table->string('aadhar_number');
            $table->string('national_id_number');
            $table->string('blood_group');
            $table->string('employee_code');
            $table->string('official_email_id');
            $table->uuid('designation_id');
            $table->unsignedBigInteger('l1_manager_id')->nullable();
            $table->unsignedBigInteger('l2_manager_id')->nullable();
            $table->string('grade');
            $table->string('joining_date');
            $table->unsignedBigInteger('hr_manager_id')->nullable();
            $table->uuid('company_id');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->foreign('designation_id')->references('id')->on('desig_mst');
            $table->foreign('company_id')->references('id')->on('company_mst');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('l1_manager_id')->references('id')->on('users');
            $table->foreign('l2_manager_id')->references('id')->on('users');
            $table->foreign('hr_manager_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
